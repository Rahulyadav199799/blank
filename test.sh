#!/bin/bash 

# Define the folders and number of files to keep for each folder
folders=(jenkins ansible)
num_files=(2 2)

# Loop through each folder and keep only the specified number of latest files
for ((i = 0; i < ${#folders[@]}; i++)); do
    cd "/opt/${folders[$i]}"
    files=($(ls -t | tail -n +$((num_files[$i] + 1))))
    
    for file in "${files[@]}"; do
        # Attempt to remove the file
        #echo "$file"
        rm -rf "$file"
        

        # Check the exit status of the rm command
        if [ $? -ne 0 ]; then
            # If the rm command failed, send an alert
            "Failed to delete file: $file in folder: ${folders[$i]}"
            exit 1
        fi
    done
done
